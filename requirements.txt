Flask==0.12.2
Flask-SQLAlchemy==2.1 # 2.2 breaks models inheriting declared_attr __tablename__
Flask-QueryInspect==0.1.2
Flask-Script==2.0.5
Flask-Assets==0.12
Flask-Admin==1.5.0
Flask-Security==3.0.0
flask-shell-ipython==0.2.2
Jinja2==2.9.6
MarkupSafe==1.0
SQLAlchemy==1.1.9
Werkzeug==0.12.2
boto==2.46.1
boto3==1.4.4
itsdangerous==0.24
python-dateutil==2.6.0
six==1.10.0
ruamel.yaml==0.14.5
