var mic_input;
var recorder;
var meter;
var analyser;
var canvasCtx;

var WIDTH = 250;
var HEIGHT = 100;

var recorder_config = {
    workerPath: '/static/lib/recorderWorker.js', //it'd be nice if this wasn't hardcoded
    numChannels: 1 //record mono sounds
};


function startRecorder(stream) {
  //make the mice input node
  mic_input = audio_context.createMediaStreamSource(stream);
  console.log('Mic stream created.');

  //start the recorder with the mixer as input
  initRecorder(mic_input);
  //start the volume analyzer for the instruction page
  initVolumeAnalyzer(mic_input);
}

function initRecorder(input) {
    recorder = new Recorder(input, recorder_config);
    console.log('Recorder initialised.');
}

function initVolumeAnalyzer(input){
  analyser = audio_context.createAnalyser();
  console.log('Analyser initialized.');

  input.connect(analyser);
  console.log('Mic connected to Analyser.');

  meter = createAudioMeter(audio_context);
  console.log('Audio meter initialized.');

  input.connect(meter);
  console.log('Mic connected to audio meter.');
}


function displayAudioMeter() {

  var canvasElement = document.getElementById('micgraph');
  canvasCtx = canvasElement.getContext("2d");

  //analyser.fftSize = 2048; // Wavform
  analyser.fftSize = 256; // frequency bar graph
  var bufferLength = analyser.frequencyBinCount;
  var dataArray = new Uint8Array(bufferLength);

  canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

  var drawWavform = function() {
    drawVisual = requestAnimationFrame(drawWavform);
    analyser.getByteTimeDomainData(dataArray);
    canvasCtx.fillStyle = 'rgb(200, 200, 200)';
    canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
    canvasCtx.lineWidth = 2;

    canvasCtx.beginPath();

    var sliceWidth = WIDTH * 1.0 / bufferLength;
    var x = 0;

    // check if we're currently clipping
    if (meter.checkClipping()) {
        canvasCtx.strokeStyle = "red";
    } else {
        canvasCtx.strokeStyle = "green";
    }

    for(var i = 0; i < bufferLength; i++) {

        var v = dataArray[i] / 128.0;
        var y = v * HEIGHT/2;

        if(i === 0) {
          canvasCtx.moveTo(x, y);
        } else {
          canvasCtx.lineTo(x, y);
        }

        x += sliceWidth;
      }

      canvasCtx.lineTo(canvasElement.width, canvasElement.height/2);
      canvasCtx.stroke();
    };

    var drawFrequencyBars = function () {
      drawVisual = requestAnimationFrame(drawFrequencyBars);

      analyser.getByteFrequencyData(dataArray);

      canvasCtx.fillStyle = 'rgb(0, 0, 0)';
      canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

      var barWidth = (WIDTH / bufferLength) * 2.5;
      var barHeight;
      var x = 0;

      // check if we're currently clipping
      if (meter.checkClipping()) {
          canvasCtx.fillStyle = "red";
      } else {
          canvasCtx.fillStyle = "green";
      }

      for(var i = 0; i < bufferLength; i++) {
        barHeight = dataArray[i]/2;

        //canvasCtx.fillStyle = 'rgb(' + (barHeight+100) + ',50,50)';
        canvasCtx.fillRect(x,HEIGHT-barHeight/2,barWidth,barHeight);

        x += barWidth + 1;
      }
    };

    //drawWavform();
    drawFrequencyBars();

}

function disconnectAudioMeter() {
  analyser.disconnect();
  // FIXME: this isn't actually disconnecting.
  console.log('Disconnected analyser.');

  canvasCtx.fillStyle = 'rgb(0, 0, 0)';
  canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
}
