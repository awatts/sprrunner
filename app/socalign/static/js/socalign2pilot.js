function skipToTrials() {
  $('#instructions').hide();
  $('#experiment_progress').val(1);
  $('.block').first().show(0);
  $('#phase_label').text('Phase 2: Memory task');
}

$(document).ready(function() {
  var navigator = window.navigator;
  var trialStarted;
  $(':checked').removeAttr('checked');
  $('input[name="browserid"]').val(navigator.userAgent);


  var listener = new window.keypress.Listener();
  listener.simple_combo('f', function() {
    console.log('y pressed');
    $('button.yes:visible').click();
  });
  listener.simple_combo('shift f', function() {
    console.log('Yes pressed');
    $('button.yes:visible').click();
  });
  listener.simple_combo('j', function() {
    console.log('n pressed ');
      $('button.no:visible').click();
  });
  listener.simple_combo('shift j', function() {
    console.log('No pressed');
      $('button.no:visible').click();
  });
  listener.stop_listening(); // Don't want to listen outside 2AFC trials


  var block1_trials = $('.block').first().children('.testtrial').map(
    function() {
      return this.id;
    }).toArray();

  var block2_trials = $('.block:nth-of-type(2)').children('.testtrial').map(
    function() {
      return this.id;
    }).toArray();

  var total_trials = block1_trials.length + block2_trials.length;
  var trial_update = 1 / total_trials;

  var trial_vals = {};

  var current_block = 1;
  var max_blocks = 2;

  if (Modernizr.audio) {
      $('#instructions').show(0);

      $('#voladjust').on('play', function() {this.volume = 1;});
      $('#voladjust').on('volumechange', function() {this.volume = 1;});
      $('#voladjust').on('ended', function() {this.currentTime = 0; this.pause();});

      $('#exposure audio').on('play', function() {
        console.log('Audio playing');
      });

      $('#exposure audio').on('ended', function() {
          console.log('Audio ended');
          $('#headphones').hide();
          $('#exposure').hide();
          $('#experiment_progress').val(1);
          $('.block').first().show(0);
          $('#phase_label').text('Phase 2: Memory task');
      });

      var v = document.getElementById('exposure_audio');
      v.addEventListener('timeupdate', function() {
        $('#experiment_progress').val(v.currentTime / v.duration);
      });
  }

  $('button#endinstr').on('click', function() {
    $('#instructions').hide();
    $('#exposure').show(function() {
        $('#phase_label').text('Phase 1: Listen to a story');
        $('#exposure audio')[0].volume = 1;
        $('#exposure audio')[0].play();
        $('#headphones').show(0);
    });
  });

  $('.yesno').on('click', function() {
    var itemId = $(this).parent().attr('id');
    trial_vals[itemId] = {
      choice: this.value,
      startTime: trialStarted.toISOString(),
      stopTime: new Date().toISOString()
    }
    listener.stop_listening();
    nextTrial();
  });

  $('.startblock').on('click', function() {
    $(this).parent().hide();
    $(this).parent().siblings('.testtrial').first().show(0, function() {
      trialStarted = new Date();
      listener.listen();
    });
  });

  var nextTrial = function() {
    var trialblock;
    if (current_block === 1) {
      trialblock = block1_trials;
    } else {
      trialblock = block2_trials;
    }
    var current = '#' + trialblock.shift();
    $(current).hide();
    var pval = $('#experiment_progress').val();
    $('#experiment_progress').val(pval + trial_update);
    if (trialblock.length === 0) {
      $('.block:nth-of-type(' + current_block + ')').hide();
      current_block++;
      if (current_block <= max_blocks) {
        $('.block:nth-of-type(' + current_block + ')').show(0);
      } else {
        $('[name="trials"]').val(JSON.stringify(trial_vals));
        $('#page1').show(0);
        $('#phase_label').text('Phase 3: Survey');
      }
    } else {
      var next = '#' + trialblock[0];
      $(next).show(0, function() {
        trialStarted = new Date();
        listener.listen();
      });
    }

  };

});
