# coding: utf-8
from app.models import Experiment, TrialList

list_names = ('list_01', 'list_01_rev',
              'list_02', 'list_02_rev',
              'list_03', 'list_03_rev',
              'list_04', 'list_04_rev')

sa2, existed = Experiment.get_one_or_create(name='socalign2')
verb = 'Got' if existed else 'Created'
print('{} socalign2 experiment'.format(verb))

for i, name in enumerate(list_names, 1):
    t, e = TrialList.get_one_or_create(name=name, number=i, experiment=sa2)
    verb = 'Got' if e else 'Created'
    print('{} socalign2 list {}, named {}'.format(verb, i, name))
