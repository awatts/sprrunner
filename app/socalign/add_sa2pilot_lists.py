# coding: utf-8
from app.models import Experiment, TrialList

s2p, _ = Experiment.get_one_or_create(name='socalign2pilot')
TrialList.get_one_or_create(name='pilot_list_01_a', number=1, experiment=s2p)
TrialList.get_one_or_create(name='pilot_list_01_b', number=2, experiment=s2p)
TrialList.get_one_or_create(name='pilot_list_02_a', number=3, experiment=s2p)
TrialList.get_one_or_create(name='pilot_list_02_b', number=4, experiment=s2p)
