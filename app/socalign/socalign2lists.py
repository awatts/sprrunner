#!/bin/env python

# Author: Andrew Watts <awatts2@ur.rochester.edu>
#
#    Copyright 2016 Andrew Watts and
#        the University of Rochester BCS Department
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License version 2.1 as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.
#    If not, see <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>.

"""
Read in Excel file with SocAlign2 lists and generates YAML trial lists
"""

from os import makedirs
from os.path import expanduser, dirname
import pandas as pd
import ruamel.yaml as yaml

instructions = {
    '2AFC_form': 'Did the speaker say the following sentence exactly in the way '
                 'shown here? If the speaker expressed the same general meaning '
                 'but used different words or a different word order, press "NO".\n'
                 'We realize that this might be very difficult to recall. Try '
                 'your best and in the worst case scenario, guess.',
    '2AFC_content': 'Did the speaker explicitly make the following argument, though '
                    'not necessarily using these exact words?',
    'sentence_completion': 'Your task is to complete the prompt below in a way that '
                           'expresses your opinion. Please read the prompt out loud, '
                           'and then continue the prompt to make a grammatically '
                           'well-formed sentence. Your response will be recorded.'
}

socalign = pd.read_excel(
    expanduser('~/Dropbox/WeatherholtzCampbell-KiblerJaeger/exp_2/lists/socalign_exp2_lists.xlsx'),
    sheetname=None)

for name, data in socalign.items():
    tasks = data['Task'].unique()  # Pandas should preserve order

    blocks = []

    for task in tasks:
        block = {}
        if task.endswith('xposure'):
            block['type'] = 'exposure'
            block['stimulus'] = data.loc[data['Task'] == 'Exposure']['Stimulus'].values[0]
        else:
            block['instructions'] = instructions[task]
            # this makes a list of dicts
            block['trials'] = data.loc[data['Task'] == task].to_dict('records')
            if task.startswith('2AFC'):
                block['type'] = '2AFC'
            elif task == 'sentence_completion':
                block['type'] = 'sentence_completion'
        block['block'] = task
        blocks.append(block)
    filename = "app/stimuli/socalign2/{}.yml".format(name)
    makedirs(dirname(filename), exist_ok=True)
    with open(filename, 'w') as yamlfile:
        yaml.dump(blocks, stream=yamlfile, default_flow_style=False)
