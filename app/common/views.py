#
# Author: Andrew Watts <awatts2@ur.rochester.edu>
#
# Copyright (c) 2014-2016, Andrew Watts and the University of Rochester BCS Department
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function
from __future__ import absolute_import
import os
# import time
from flask import Blueprint, request, render_template, redirect, abort, url_for
from flask_admin.contrib.sqla import ModelView
import flask_login as login
from flask_security import login_required
from werkzeug.exceptions import BadRequest
from sqlalchemy.orm.exc import NoResultFound
from itsdangerous import URLSafeSerializer, BadSignature
from app.models import Experiment, TrialList, NotificationEvent, Protocol, User, Role
from app import app, db, admin

mod = Blueprint('common',
                __name__,
                template_folder='templates',
                static_folder='static',
                static_url_path='/static/common')

# we want the 'app' directory to be the base
basepath = os.path.split(os.path.dirname(__file__))[0]


@app.errorhandler(400)
def bad_request(error):
    return render_template('400.html', error=error), 400


@app.route('/')
def app_root():
    return render_template('home.jinja2')


@mod.route('/experiments')
@login_required
def experiments():
    return render_template('experiments.jinja2', experiments=Experiment.query.all())


@mod.route('/experiments/<experiment>')
@login_required
def experiment_detail(experiment):
    try:
        e = Experiment.query.filter_by(name=experiment).one()
        return render_template('experiment_detail.jinja2', experiment=e)
    except NoResultFound:
        abort(400, {'message': 'No such experiment as {}'.format(experiment)})


#  http://stackoverflow.com/a/13318415/3846301
def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@mod.route('/site-map')
@login_required
def site_map():
    links = []
    for rule in app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint)
            links.append((url, rule.endpoint))
            # links is now a list of url, endpoint tuples
    return render_template("all_links.jinja2", links=links)


@mod.route('/echo/', methods=['POST'])
def response_repeater():
    """
    Take all the keys and values in a request object and print them in a table
    """
    return render_template('echo.jinja2', formdata=request.form)


@app.context_processor
def utility_processor() -> dict:
    return dict(get_upload_link=get_upload_link)


# Some parts based on http://flask.pocoo.org/snippets/50/
def get_serializer(secret_key=None):
    if secret_key is None:
        secret_key = app.secret_key
    return URLSafeSerializer(secret_key)


def get_upload_link(experiment, workerid, assignmentid, trial):
    s = get_serializer()
    payload = s.dumps((experiment, workerid, assignmentid, trial))
    return url_for('common.wav_uploader', payload=payload, _external=True)


@mod.route('/wav_uploader/<payload>', methods=['PUT', ])
def wav_uploader(payload):
    """
    Handle WAV uploads
    """
    # TODO: should only allow XHR
    # print(request.headers)
    s = get_serializer()
    try:
        experiment, workerid, assignmentid, trial = s.loads(payload)
        print('{},{},{}'.format(workerid, assignmentid, trial))
    except BadSignature:
        print('bad signature')
        raise BadRequest('Invalid file signature')

    if request.content_type != 'audio/x-wav':
        print('need wavs')
        raise BadRequest('Only WAV files can be uploaded')

    savepath = os.path.join(basepath, 'uploads', experiment, workerid, assignmentid)
    savefile = trial + ".wav"

    if not os.path.exists(savepath):
        os.makedirs(savepath)

    with open(os.path.join(savepath, savefile), 'wb') as wavfile:
        wavfile.write(request.data)

    return 'Uploaded "{}"'.format(savefile)


class AuthorizedModelView(ModelView):
    def is_accessible(self):
        return login.current_user.has_role('admin') or login.current_user.has_role('editor')

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('security.login', next=request.url))


class NotificationView(AuthorizedModelView):
    can_create = False
    can_edit = False
    can_delete = False
    column_filters = ['event_type', 'event_time']


class UserView(AuthorizedModelView):
    column_exclude_list = ('password', )
    column_hide_backrefs = False
    column_list = ('email', 'active', 'confirmed', 'roles')


admin.add_view(AuthorizedModelView(Protocol, db.session))
admin.add_view(AuthorizedModelView(Experiment, db.session))
admin.add_view(AuthorizedModelView(TrialList, db.session))
admin.add_view(NotificationView(NotificationEvent, db.session))
admin.add_view(UserView(User, db.session))
admin.add_view(AuthorizedModelView(Role, db.session))


