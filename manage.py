#!/usr/bin/env python3

from app import app
from app import models
from app import db

# from flask import url_for
from flask_script import Manager

manager = Manager(app)


@manager.shell
def make_shell_context():
    return dict(app=app, db=db, models=models)


@manager.command
def list_routes():
    # import urllib
    # :woutput = []
    for rule in app.url_map.iter_rules():
        methods = ','.join(rule.methods)
        print("'{}' ({}) -> {}".format(rule.rule, methods, rule.endpoint))

#        options = {arg: "[{0}]".format(arg) for arg in rule.arguments}

#        url = url_for(rule.endpoint, **options)
#        line = urllib.parse.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
#        output.append(line)

#    for line in sorted(output):
#        print(line)


if __name__ == '__main__':
    manager.run()
