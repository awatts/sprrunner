#!/usr/bin/env python

from getpass import getpass
from sys import exit
from flask_security.utils import encrypt_password
from sqlalchemy.exc import IntegrityError
from app import app, db, user_datastore

app.config['QUERYINSPECT_ENABLED'] = False

user_email = input("User email address: ")
password1 = getpass("Password: ")
password2 = getpass("Re-enter password: ")

if password1 != password2:
    print('Passwords don\'t match!')
    exit()

with app.app_context():
    user = user_datastore.create_user(email=user_email, password=encrypt_password(password1))
    try:
        db.session.commit()
    except IntegrityError:
        print('User {} already exists!'.format(user.email))
    else:
        print('Created user {}'.format(user.email))
